<?php 
    require_once(__DIR__.'/functions.php');       
    if(!empty($_GET)&&array_key_exists('test', $_GET)){
        $arr = glob(__DIR__ . '/tests/*'.$_GET['test'].'_.json');
        if(count($arr) === 0){
            http_response_code(404);
        } 
    }
    if(!empty($_POST)){
        if(array_key_exists('user', $_POST)){
            $userName = $_POST['user'];
        } 
       $good = 'Вы верно ответили на вопрос(ы): <br>';
       $bad = 'Вы не верно ответили на вопрос(ы): <br>';
       $countGood=0;  
       $countBad=0;        
        $testQuery = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        $testName = getTestNamefromURL();
       $fileName =  getTestFileNamefromURL();
       $questionArray =  getJson($fileName);
       
       foreach($_POST as $key => $value){
        if($key === 'user') continue;
         if($questionArray["questions"][$key]['true'] == $value){
                    $good .= $questionArray["questions"][$key]['questionName'] ;
                    $good .= ' (Верный ответ  '.$questionArray["questions"][$key]['true'].'); <br>';
                    $countGood++;
         }else{
                    $bad .= $questionArray["questions"][$key]['questionName'] . ' (Верный ответ  '.$questionArray["questions"][$key]['true'].'); <br>';
                     $countBad++;
           }
        }
    /*    echo '<pre>';
        var_dump($good);
         var_dump($bad);*/
        $_SESSION["text_1"] = 'Уважаемый, '. $userName.', '; 
        $_SESSION["text_2"] = 'в тесте "'. $testName.'"'; 
        $_SESSION["text_3"] = 'Вы правильно ответили на '. $countGood .' вопросов';    
        $_SESSION["text_4"] = 'Вы не правильно ответили на '. $countBad .' вопросов'; 
        echo '<img style="display: block; margin: 30px auto;" src="sertif.php">';
    }
   ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
    <?php 
    if(!empty($_GET)&&array_key_exists('test', $_GET) && empty($_POST)){
        $name = getTestNamefromGet($_GET['test']); 
        if($name === ''){
            http_response_code(404);
        } else{
           echo   '<h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Тест № '.  $_GET['test'].'</h1>';
           $questionArray =  getJson($name);
          echo '<h2 style="width: 800px; margin: 0px auto 30px; text-align:center">'. $questionArray['testName'].'</h2>';
            echo '<form style="width: 400px; margin: 0px auto;" action="" method="POST">';
            echo '<fieldset style="margin-bottom: 20px;"><legend style="margin-bottom: 15px;">Введите Ваше Имя </legend>';
            echo '<label  style="margin-right: 30px;"><input style="margin-right: 10px;" type="text" name="user" required></label>';
            if(!empty($questionArray)){
                $count = 1;
                foreach($questionArray['questions'] as $question => $questionDetails){                  
                     echo '<fieldset style="margin-bottom: 20px;"><legend style="margin-bottom: 15px;">'.$questionDetails['questionName'].'</legend>';
                        foreach($questionDetails as $key => $questionDetail){
                            if($key == 'questionName') continue;
                            if($key == 'true' ) continue;
                            echo '<label  style="margin-right: 30px;"><input style="margin-right: 10px;" type="radio" name="q'.$count.'" value="'.$questionDetail. '">'. $questionDetail .'</label>';
                        }
                     echo '</fieldset>';
                     $count++;
                }
            }     
            echo  '<button type="submit" class="btn btn-primary">Отправить</button>';
            echo '</form>'; 
        }
    }  
        ?>
    </body>
</html>    
	


